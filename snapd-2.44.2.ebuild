# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit autotools bash-completion-r1 golang-base golang-vcs-snapshot eutils linux-info systemd xdg-utils

DESCRIPTION="Service and tools for management of snap packages"
HOMEPAGE="http://snapcraft.io/"
SRC_URI="https://github.com/snapcore/${PN}/releases/download/${PV}/${PN}_${PV}.vendor.tar.xz -> ${P}.tar.xz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="apparmor nvidia-biarch bash-completion"

PKG_LINGUAS="am bs ca cs da de el en_GB es fi fr gl hr ia id it ja lt ms nb oc pt_BR pt ru sv tr ug zh_CN"

DEPEND=">=dev-lang/go-1.9
        dev-vcs/git
        dev-vcs/bzr
        sys-apps/coreutils
        sys-libs/libcap"

RDEPEND="sys-libs/libseccomp
        sys-apps/systemd
        sys-auth/polkit
        apparmor? ( sys-apps/libapparmor )
        bash-completion? ( app-shells/bash-completion )
        sys-fs/squashfs-tools
        sys-fs/xfsprogs"

MY_S="${S}/src/github.com/snapcore/${PN}"
EGO_PN="github.com/snapcore/${PN}"

pkg_setup() {
        CONFIG_CHECK="~BLK_DEV_LOOP
            ~CGROUPS
            ~CGROUP_DEVICE
            ~CGROUP_FREEZER
            ~NAMESPACES
            ~SECCOMP
            ~SECCOMP_FILTER
            ~SQUASHFS
            ~SQUASHFS_XZ
            ~SQUASHFS_ZLIB
            ~SQUASHFS_LZO"

    if use apparmor ; then
        CONFIG_CHECK="$CONFIG_CHECK
            ~SECURITY_APPARMOR"
    fi
                
        linux-info_pkg_setup
}

export GOPATH="${S}/${PN}"

src_prepare() {
        cd "${MY_S}"
        einfo "Patching dirs/dirs.go..."
        sed -i 's/"manjaro", "antergos"/"manjaro", "gentoo", "antergos"/g' dirs/dirs.go
        ./mkversion.sh ${PV}
        cd cmd
        default
        eautoreconf
}

src_configure() {
        cd ${MY_S}/cmd
        local myconf=(
                --libexecdir=/usr/lib/snapd
                --with-snap-mount-dir=/var/lib/snapd/snap
                --enable-merged-usr
                $(use_enable apparmor)
                $(use_enable nvidia-biarch)
        )
        econf "${myconf[@]}" || die
}

src_compile() {
        emake -C ${MY_S}/data \
        BINDIR=/usr/bin \
        LIBEXECDIR=/usr/lib \
        SNAP_MOUNT_DIR=/var/lib/snapd/snap \
        SNAPD_ENVIRONMENT_FILE=/etc/default/snapd || die
        emake -C ${MY_S}/cmd || die

        export GOPATH="${S}/"
        export CGO_ENABLED="1"
        export CGO_CFLAGS="${CFLAGS}"
        export CGO_CPPFLAGS="${CPPFLAGS}"
        export CGO_CXXFLAGS="${CXXFLAGS}"
        export CGO_LDFLAGS="${LDFLAGS}"
        export BUILD_ID="-B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' '\\n)"
        for BIN in snap snapctl snapd snap-seccomp snap-failure snap-update-ns snap-exec; do
            einfo "Building ${BIN}"
            go install -ldflags "$BUILD_ID" -v "github.com/snapcore/${PN}/cmd/${BIN}" || die
        done

        einfo "Compiling lang files"
        for I in ${PKG_LINGUAS};do
            msgfmt -v --output-file="${MY_S}/po/${I}.mo" "${MY_S}/po/${I}.po"
        done
}

src_install() {
        keepdir "/var/lib/${PN}/void"
        emake -C ${MY_S}/data/ install \
        DBUSSERVICESDIR=/usr/share/dbus-1/services \
        BINDIR=/usr/bin \
        SNAP_MOUNT_DIR=/var/lib/snapd/snap \
        DESTDIR="${D}" || die

        if use bash-completion; then
            dobashcomp ${MY_S}/data/completion/snap || die
        fi

        insinto "/usr/share/dbus-1/services/"
        doins ${MY_S}/data/dbus/io.snapcraft.Launcher.service
        insinto "/usr/share/polkit-1/actions/"
        doins ${MY_S}/data/polkit/io.snapcraft.snapd.policy
        
        # Install executables
        exeinto /usr/lib/${PN}
        for BIN in snapd snap-failure snap-seccomp snap-update-ns snap-exec; do
            doexe ${S}/bin/$BIN || die
        done
        dobin ${S}/bin/snap
        dobin ${S}/bin/snapctl
        # Symlink /var/lib/snapd/snap to /snap so that --classic snaps work
        #ln -s var/lib/snapd/snap "${D}/snap"

        # pre-create directories
        for DIR in snap assertions desktop/applications device hostfs mount seccomp/bpf snap/bin snaps void\
        lib/gl lib/gl32 lib/vulkan; do
            install -dm755 "${D}/var/lib/snapd/$DIR" || die
        done
        # these dirs have special permissions
        install -dm700 "${D}/var/lib/snapd/cookie"
        install -dm700 "${D}/var/lib/snapd/cache"
        for DIR in snap assertions desktop/applications device hostfs mount seccomp/bpf snap/bin snaps \
        lib/gl lib/gl32 lib/vulkan void cookie cache; do
            keepdir "/var/lib/snapd/$DIR"
        done
        make -C ${MY_S}/cmd install DESTDIR="${D}/" || die

        # Install man file
        mkdir -p "${D}/usr/share/man/man1"
        "bin/snap" help --man > "${D}/usr/share/man/man1/snap.1"

        # Install the "info" data file with snapd version
        install -m 644 -D ${MY_S}/data/info \
            "${D}/usr/lib/snapd/info"

        # Remove snappy core specific units
        rm -fv "${D}/lib/systemd/system/snapd.system-shutdown.service"
        rm -fv "${D}/lib/systemd/system/snapd.autoimport.service"
        rm -fv "${D}/lib/systemd/system/"snapd.snap-repair.*
        rm -fv "${D}/lib/systemd/system/"snapd.core-fixup.*
        # and scripts
        rm -fv "${D}/lib/snapd/snapd.core-fixup.sh"
        rm -fv "${D}/bin/ubuntu-core-launcher"
        rm -fv "${D}/lib/snapd/system-shutdown"
        
        domo ${MY_S}/po/*.mo

        if ! use apparmor; then
            rm -rvf "${D}/var/lib/${PN}/apparmor"
        else
            keepdir "/var/lib/${PN}/apparmor/snap-confine"
                fi
        
}

pkg_postinst() {
        xdg_desktop_database_update
}

pkg_postrm() {
        xdg_desktop_database_update
}